<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function()
{
    return view('welcome');
});

Route::get('startgame','GameController@start')->name('startgame')->middleware('auth')->middleware('verifyMiddleware');


Route::post('/data','GameController@getBoardPiece')->middleware('auth')->middleware('verifyMiddleware');


Route::get('game','GameController@showGame')->name('showGame')->middleware('auth');


Route::any('/move','GameController@startMove')->middleware('auth')->middleware('verifyMiddleware');


Route::get('/user/verify/{email_token}','VerifyController@verifyMail');

Route::get('/verify',function()
{
    return view('sendLink');
});

Route::get('/admin',function()
{
    return view('adminView');
})->name('admin')->middleware('auth')->middleware('AdminMiddleware');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
