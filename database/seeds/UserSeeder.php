<?php

use App\boardpiece;
use App\Piece;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $boardId;
    protected $boardRows;
    protected $boardColumns;
    //protected $pieces;

    public function run()
    {
        factory(App\User::class)->create();
    }
}