<?php

use Illuminate\Database\Seeder;


class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();

        foreach($users as $user) {
            for ($games = 1; $games <= 3; $games++) {
                $game = new \App\Game;
                $game->status = true;
                $game->user_id = $user->id;
                $game->save();
            }
        }
    }
}
