<?php

use Illuminate\Database\Seeder;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = \App\Game::all();

        foreach($games as $game) {
            $board = new \App\Board();
            $board->rows = mt_rand(4, 5);
            $board->columns = mt_rand(4, 5);
            $board->game_id = $game->id;
            $board->save();
        }
    }
}
