<?php

use Illuminate\Database\Seeder;

class PieceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boards = \App\Board::all();

        foreach($boards as $board)
        {
            $totalPiece = mt_rand(2,3);
            echo "  total pieces =>".$totalPiece;

            for($pieces = 1;$pieces<=$totalPiece;$pieces++)
            {
                $x = mt_rand(1,$board->rows);
                $y = mt_rand(1,$board->columns);

                $existPieces = \App\Piece::all();
                echo " x=>".$x." y=>".$y;

                if($existPieces->isNotEmpty())
                {
                    foreach($existPieces as $existPiece)
                    {
                        $oldPiece = \App\Piece::where('x',$x)->where('y',$y)->first();
                        if(!$oldPiece)
                        {
                            $piece = new \App\Piece;
                            $piece->x = $x;
                            $piece->y = $y;
                            $piece->save();

                            factory(App\boardpiece::class)->create(['board_id'=>$board->id,'piece_id'=>$piece->id,'x'=>$x,'y'=>$y]);
                            break;
                        }
                        else{
                            $exist = \App\boardpiece::where('board_id',$board->id)->where('x',$x)->where('y',$y)->first();
                            if(!$exist) {
                                factory(App\boardpiece::class)->create(['board_id'=>$board->id,'piece_id'=>$oldPiece->id,'x'=>$x,'y'=>$y]);
                            }
                            else{
                                $pieces--;
                            }
                            break;
                        }
                    }
                }
                else{
                    $piece = new \App\Piece;
                    $piece->x = $x;
                    $piece->y = $y;
                    $piece->save();

                    factory(App\boardpiece::class)->create(['board_id'=>$board->id,'piece_id'=>$piece->id,'x'=>$x,'y'=>$y]);
                }
            }
        }

    }
}
