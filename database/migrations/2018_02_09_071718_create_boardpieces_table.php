<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardpiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardpieces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('board_id')->references('id')->on('boards');
            $table->integer('piece_id')->references('id')->on('pieces');
            $table->integer('x');
            $table->integer('y');
            $table->string('commands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardpieces');
    }
}
