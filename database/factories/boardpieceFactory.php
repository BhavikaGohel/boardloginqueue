<?php

use Faker\Generator as Faker;

$factory->define(App\boardpiece::class, function (Faker $faker) {

    $moves = ['up','down','left','right'];

    return [
        'commands'=> implode(',',$faker->randomElements($moves,mt_rand(1,4)))
    ];
});
