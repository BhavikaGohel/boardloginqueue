
@extends('layouts.app')
<script>
     var countBox = 1;
     function addPiece() {
         var x = "pieces[" + countBox +"][x]";
         var y = "pieces[" + countBox +"][y]";
         var command = "pieces[" + countBox +"][command]";
         document.getElementById('result').innerHTML
             += '<br>'+
             'Piece '+ countBox +'<br/>' +
             '   x    : <input type="text" name="' + x + '" "  /><br/>' +
             '   y    : <input type="text" name="' + y + '" "  /><br/>' +
             'command : <input type = "text" name="'+command + '" " /><br/>';
            countBox += 1;
     }

</script>
@section('content')
<html>
<body>
    <center>
        <form name = "board" method = "post" action = "/data">
            {{csrf_field()}}

            <fieldset style="width:400px">
                <legend>New Board</legend>
                Number of Rows    : <input type = "text" name = "row" /><br><br>
                Number of Columns : <input type = "text" name = "column" /><br><br>
            </fieldset>
            <br>
            <fieldset style = "width:400px">
                <legend>Create Piece</legend>
                <input type = "button" name = "addpiece" value = "ADD PIECE" onclick="addPiece()"><br>
                <span id = "result"></span><br>
            </fieldset>
            <center><input type = "submit" name = "start" value = "START GAME"/></center>
        </form>
    </center>
</body>
</html>
@endsection
