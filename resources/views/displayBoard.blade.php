@extends('layouts.app')
<html>
<head>
    <style>
        td{
            height:60px;
            width:60px;
            border-width:2px;
            text-align: center;
        }
    </style>
</head>
<body>

@section('content')
<center>
@if(session()->has('invalidPiece'))
        <h3 style="color:red;"> {{ session()->get('invalidPiece') }} </h3>
@endif
<table border ='1' cellpadding='20', cellspacing='5'>
    @for($row = 1; $row <= $board->rows;$row++)
        <tr>
            @for($column = 1;$column <= $board->columns ;$column++)
                @foreach($pieces as $piece)
                    @if($row==$piece->x && $column==$piece->y)
                        @php $flag =1; @endphp
                        @break
                    @else
                        @php $flag =0; @endphp
                    @endif
                @endforeach

                @if($flag == 1)
                    <td>1</td>
                @else
                    <td> </td>
                @endif
            @endfor
        </tr>
    @endfor
</table>
<br>

<form name = "play" method = "post" action = "/move">
    {{csrf_field()}}
    <input type = "submit" name = "start" value = " PLAY " />
    <br><br>
    <input type = "submit" name = "newgame" value = "NEW GAME"/>
</form>

@if(session()->has('error'))
    <h3 style="color:red;"> {{ session()->get('error') }} </h3>
@endif
@if(session()->has('invalidMove'))
    <h3 style="color:red;"> {{ session()->get('inValidMove') }} </h3>
@endif



</center>
@endsection
</body>
</html>