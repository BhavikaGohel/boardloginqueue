<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyController extends Controller
{
    public function verifyMail(Request $request, $verificationCode)
    {
        //check if verificationCode exists
        if (!$valid = User::where('email_token', $verificationCode)->first()) {
            return redirect('/login');
        }

        $conditions = [
            'verified_at' => null,
            'email_token' => $verificationCode
        ];

        if ($valid = User::where($conditions)->first()) {

            $valid->verified_at = date("Y-m-d h:i:sa");
            $valid->save();

            $created_at = strtotime($valid->created_at);
            $verified_at = strtotime($valid->verified_at);

            if($verified_at-$created_at < 500) {
                Auth::logout();
                return redirect('/login');
            }
            else{
                User::where('email_token', $valid->email_token)->delete();
                $request->session()->flash('error','Verification Timeout, Please register again..');
                return redirect('/register');
            }
        }
        return redirect('/home');
    }
}
