<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Board;
use App\Piece;
use App\Game;
use App\boardpiece;
use App\Moves;

class GameController extends Controller
{
    function start()
    {
        $user = Auth::user();
        $game = $user->game()->where('status', true)->first();
        if($game)
        {
            return redirect("/game");
        }
        else{
            return view("getBoardValue");
        }

    }
    function getBoardPiece(Request $request)
    {
        $maingame = new Game;
        $maingame->status = true;
        $maingame->user_id=Auth::user()->id;
        $maingame->save();

        $gameId = $maingame->id;

        //insert into board table
        $board = new Board;
        $board->rows = $request->row;
        $board->columns = $request->column;
        $board->game_id = $gameId;
        $board->save();

        //insert into piece table,retrieve all pieces
        $x = [];
        $y = [];
        $pieceId = [];
        $command = [];

        $pieces = $request->input('pieces');
        foreach($pieces as $piece)
        {
            //$piece = piece::firstOrCreate(['x'=>$piece['x'], 'y'=>$piece['y']]);
            $existPieces = Piece::all();
            if($existPieces->isNotEmpty())
            {
                foreach ($existPieces as $existPiece) {
                    $oldPiece = Piece::where('x', $piece['x'])->where('y', $piece['y'])->first();
                    if (!$oldPiece) {
                        $newPiece = new Piece;
                        $newPiece->x = $piece['x'];
                        $newPiece->y = $piece['y'];
                        $newPiece->save();
                    }
                }
            }
            else{
                $newPiece = new Piece;
                $newPiece->x = $piece['x'];
                $newPiece->y = $piece['y'];
                $newPiece->save();
            }

            $singlePiece = Piece::where('x',$piece['x'])->where('y',$piece['y'])->first();

            $pieceId[] = $singlePiece->id;
            $x[] = $singlePiece->x;
            $y[] = $singlePiece->y;
            $command[] = $piece['command'];
        }
        //insert into boardpiece

        for($count = 0;$count<count($pieceId);$count++) {
            $exists = boardpiece::where('board_id',$board->id)->where('x',$x[$count])->where('y',$y[$count])->first();
            if(!$exists) {
                boardpiece::create(
                    ['board_id' => $board->id,
                        'piece_id' => $pieceId[$count],
                        'x' => $x[$count],
                        'y' => $y[$count],
                        'commands' => $command[$count]]);
            }
        }
        return redirect('/game');
    }

    public function showGame()
    {
        $user = Auth::user();

        $game = $user->game()->where('status',true)->first();
        $board = Board::where('game_id',$game->id)->first();
        $pieces = boardpiece::where('board_id',$board->id)->get();

        return view('displayBoard',['board'=>$board,'pieces'=>$pieces]);
    }

    function startMove(Request $request)
    {
        $user = Auth::user();
        $game = $user->game()->where('status',true)->first();
        if(!$game)
        {
            return view("getBoardValue");
        }
        $board = Board::where('game_id',$game->id)->first();
        $pieces = boardpiece::where('board_id', $board->id)->get();

        $isMove = 0;
        foreach ($pieces as $piece) {
            $pieceMoves = explode(',', $piece->commands);
            $pieceMove = array_shift($pieceMoves);
            $remainingMoves = implode(',', $pieceMoves);
            if ($pieceMove) {
                $isMove++;
                $isUnique = true;
                if ($pieceMove == "up") {
                    if (!($piece->x <= 1)) {
                        $piece->x -= 1;
                    } else {
                        boardpiece::where('piece_id', $piece->piece_id)->where('board_id', $board->id)
                            ->update(['commands' => '']);
                        $isUnique = false;
                        echo '<script language="javascript"> alert("can not move up") </script>';
                    }
                }
                if ($pieceMove == "down") {
                    if (!($piece->x >= $board->rows)) {
                        $piece->x += 1;
                    } else {
                        boardpiece::where('piece_id', $piece->piece_id)->where('board_id', $board->id)
                            ->update(['commands' => '']);
                        $isUnique = false;
                        echo '<script language="javascript"> alert("can not move down") </script>';
                    }
                }
                if ($pieceMove == "left") {
                    if (!($piece->y <= 1)) {
                        $piece->y -= 1;
                    } else {
                        boardpiece::where('piece_id', $piece->piece_id)->where('board_id', $board->id)
                            ->update(['commands' => '']);
                        $isUnique = false;
                        echo '<script language="javascript"> alert("can not move left") </script>';
                    }
                }
                if ($pieceMove == "right") {
                    if (!($piece->y >= $board->columns)) {
                        $piece->y += 1;
                    } else {
                        boardpiece::where('piece_id', $piece->piece_id)->where('board_id', $board->id)
                            ->update(['commands' => '']);
                        $isUnique = false;
                        echo '<script language="javascript"> alert("can not move right") </script>';
                    }
                }
                $move = new Moves;
                $move->board_id = $board->id;
                $move->piece_id = $piece->id;
                $move->command = $pieceMove;
                $move->save();


                if ($isUnique == true) {
                    boardpiece::where('board_id', $board->id)->where('piece_id', $piece->piece_id)
                        ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => $remainingMoves]);
                }
            }
        }
        if ($isMove) {
            ?>
            <script>
                setTimeout(function () {
                    window.location.reload(1);
                }, 3000);
            </script>
            <?php
        } else {
            $request->session()->flash('error', "Game Over");
            Game::where(['id' => $game->id])->update(['status' => false]);
        }
        $latestPieces = boardpiece::where('board_id', $board->id)->get();
        return view('displayBoard', ['board' => $board, 'pieces' => $latestPieces]);
    }
}