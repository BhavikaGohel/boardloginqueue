<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boardpiece extends Model
{
     protected $fillable = array(
        'board_id',
        'piece_id',
        'x',
        'y',
        'commands'
    );

    public function board()
    {
        return $this->belongsTo('App\Board');
    }

    public function piece()
    {
        return $this->belongsTo('App\Piece');
    }

    public $timestamps = false;
}
