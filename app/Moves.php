<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moves extends Model
{
    protected $fillable=array(
        'board_id',
        'piece_id',
        'command'
    );

    public function board()
    {
        return $this->belongsTo('App\Board');
    }

    public function piece()
    {
        return $this->belongsTo('App\Piece');
    }
    /*public function createMoves($boardId,$pieceId,$command)
    {
      return[
          'board_id'=>$boardId,
          'piece_id'=>$pieceId,
          'command'=>$command
      ];
    }  */
}
