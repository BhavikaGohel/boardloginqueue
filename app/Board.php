<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = array(
        'rows',
        'columns',
        'game_id'
    );

    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    public function boardPiece()
    {
        return $this->hasMany('App\BoardPiece');
    }

    public function moves()
    {
        return $this->hasMany('App\Moves');
    }

    public $timestamps = false;
}
