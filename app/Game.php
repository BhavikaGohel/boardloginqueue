<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable=array(
      'status',
      'user_id'
    );

    public function board()
    {
        return $this->hasOne('App\Board');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public $timestamps = false;
}
