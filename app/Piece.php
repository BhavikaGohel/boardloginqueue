<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected $fillable=array(
        'x',
        'y'
    );

    public function boardPiece()
    {
        return $this->hasMany('App\BoardPiece');
    }

    public function moves()
    {
        return $this->hasMany('App\Moves');
    }

    public $timestamps = false;
}
